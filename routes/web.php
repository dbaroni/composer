<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//prende l'elenco degli autori
$router->get('/authors', function () use ($router) {
    //seleziono tutti gli autori
    $result = app('db') -> select("SELECT * FROM authors");
    return $result;
});

$router->get('/books', function () use ($router) {
    //seleziono tutti gli autori
    $result = app('db') -> select("SELECT * FROM books");
    return $result;
});

$router->get('/genres', function () use ($router) {
    //seleziono tutti gli autori
    $result = app('db') -> select("SELECT * FROM genres");
    return $result;
});


//inserire un nuovo autore
$router -> post('/authors', 'AuthorController@add');
$router -> put('/authors/{id}', 'AuthorController@update');
$router -> delete('/authors/{id}', 'AuthorController@delete');
$router -> get('/authors/{id}', 'AuthorController@select');

$router -> post('/books', 'BookController@add');
$router -> put('/books/{id}', 'BookController@update');
$router -> delete('/books/{id}', 'BookController@delete');
$router -> get('/books/{id}', 'BookController@select');

$router -> post('/genres', 'GenreController@add');
$router -> put('/genres/{id}', 'GenreController@update');
$router -> delete('/genres/{id}', 'GenreController@delete');
$router -> get('/genres/{id}', 'GenreController@select');