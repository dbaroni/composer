<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorController extends Controller
{

    public function add(Request $request)
    {
        $name = $request->input('name');
        $birth = $request->input('birth');
        $nationality = $request->input('nationality');

        $result = app('db')->select(
            "INSERT INTO authors(name,birth,nationality) values('$name','$birth','$nationality')"
        );

        //return 201 created
        return new Response(null, 201);
    }

    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $birth = $request->input('birth');
        $nationality = $request->input('nationality');

        $result = app('db')->select(
            "UPDATE authors SET name=$name,birth=$birth,nationality='$nationality' WHERE id=$id"
        );
        //return 200 OK
        return new Response(null, 200);
    }

    public function delete(Request $request, $id)
    {
        $result = app('db')->select(
            "DELETE from authors WHERE id=$id"
        );
        
        //return 200 OK
        return new Response(null, 200);

    }

    public function select(Request $request, $id)
    {

        $result = app('db')->select(
            "SELECT * from authors WHERE id=$id"
        );
        
        //return 200 OK
        return $result;

    }
}
