<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GenreController extends Controller
{

    public function add(Request $request)
    {
        $description = $request->input('description');
        $genre = $request->input('genre');

        $result = app('db')->select(
            "INSERT INTO genres(description,genre) values('$description','$genre')"
        );

        //return 201 created
        return new Response(null, 201);
    }

    public function update(Request $request, $id)
    {
        $description = $request->input('description');
        $genre = $request->input('genre');

        $result = app('db')->select(
            "UPDATE genres SET description=$description,genre=$genre' WHERE id=$id"
        );
        //return 200 OK
        return new Response(null, 200);
    }

    public function delete(Request $request, $id)
    {
        $result = app('db')->select(
            "DELETE from genres WHERE id=$id"
        );
        
        //return 200 OK
        return new Response(null, 200);

    }

    public function select(Request $request, $id)
    {

        $result = app('db')->select(
            "SELECT * from genres WHERE id=$id"
        );
        
        //return 200 OK
        return $result;

    }
}
