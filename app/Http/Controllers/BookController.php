<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookController extends Controller
{

    public function add(Request $request)
    {
        $price = $request->input('price');
        $title = $request->input('title');
        $year = $request->input('year');

        $result = app('db')->select(
            "INSERT INTO books(price,title,year) values('$price','$title','$year')"
        );

        //return 201 created
        return new Response(null, 201);
    }

    public function update(Request $request, $id)
    {
        $price = $request->input('price');
        $title = $request->input('title');
        $year = $request->input('year');

        $result = app('db')->select(
            "UPDATE books SET price=$price,title=$title,year='$year' WHERE id=$id"
        );
        //return 200 OK
        return new Response(null, 200);
    }

    public function delete(Request $request, $id)
    {
        $result = app('db')->select(
            "DELETE from books WHERE id=$id"
        );
        
        //return 200 OK
        return new Response(null, 200);

    }

    public function select(Request $request, $id)
    {

        $result = app('db')->select(
            "SELECT * from books WHERE id=$id"
        );
        
        //return 200 OK
        return $result;

    }
}
